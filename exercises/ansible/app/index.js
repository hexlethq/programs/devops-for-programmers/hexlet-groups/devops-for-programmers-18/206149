const fastify = require('fastify')

fastify.get('/', (req, reply) => {
  reply.send({ hello: 'world' })
})

fastify.listen(8080)
